﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class DayNightCycle : MonoBehaviour {

	public float time;
	public TimeSpan CurrentTime;
	public Transform SunTransform;
	public Light Sun;
	public Text timetxt;
	public Text daytxt;
	public int days;
	public float intensity;
	public Color fogday = Color.grey;
	public Color fognight = Color.black;
	public int speed;


	// Update is called once per frame
	void Update () {
		ChangeTime ();
		daytxt.text = "Day:" + days;
	}


	public void ChangeTime()
	{
		time += Time.deltaTime * speed;
		if (time > 86400) // The amount of seconds in a day.
		{
			days += 1;
			time = 0;
		}

		CurrentTime = TimeSpan.FromSeconds (time);
		string[] temptime = CurrentTime.ToString ().Split (":"[0]);
		timetxt.text = temptime [0] + ":" + temptime [1];

		SunTransform.rotation = Quaternion.Euler (new Vector3((time-21600)/86400*360,0,0)); // 21600 is 1/4 of a day, divided by the amount of seconds in a day. Multiply the rotation of the sun
		if (time < 43200) // Halfway through the day
			intensity = 1 - (43200 - time) / 43200;
		else
			intensity = 1 - ((43200 - time) / 43200 * -1);
		//RenderSettings.fogColor = Color.Lerp (fognight, fogday, intensity * intensity);

		Sun.intensity = intensity;
	}

}
