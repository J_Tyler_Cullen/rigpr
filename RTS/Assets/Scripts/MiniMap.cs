﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour {

	public Transform Proto_Player;
	public Transform Proto_Player2;
	public GameObject P1;
	public GameObject P2;

	//Is called after updated and fixed update, therefore the minimap will only update once the player has moved

	void LateUpdate ()
	{
		if (P1.activeInHierarchy == true) {

			Vector3 NewPos = Proto_Player.position;
			//Stay Zoomed out
			NewPos.y = transform.position.y;
			transform.position = NewPos;

			// If we have the player rotating, the bellow script will allow the minimap to rotate with it.

			transform.rotation = Quaternion.Euler (90f, Proto_Player.eulerAngles.y, 0f);
		}

		if (P2.activeInHierarchy == true) {

			Vector3 NewPos2 = Proto_Player2.position;
			//Stay Zoomed out
			NewPos2.y = transform.position.y;
			transform.position = NewPos2;

			// If we have the player rotating, the bellow script will allow the minimap to rotate with it.

			transform.rotation = Quaternion.Euler (90f, Proto_Player2.eulerAngles.y, 0f);
		}
	}

}
