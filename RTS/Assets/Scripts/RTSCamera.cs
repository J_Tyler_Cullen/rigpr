﻿using UnityEngine;

public class RTSCamera : MonoBehaviour {

	public float Speed_Pan = 20.0f;
	// This variable checks if the mouse is within 10 pixels of the border of the screen. If so then the camera will move (as shown in the input code)
	public float Pan_Border = 10.0f;
	public Vector2 Pan_Limit;
	public float Speed_Scroll = 20.0f;
	public float Min_Y = 20.0f;
	public float Max_Y = 120f;
	//public Transform FocusPoint;
	//public Vector3 offset;
	//public float pitch = 2f;

	//private float DefaultZoom = 10f;



	// Update is called once per frame
	void Update () {


		// Cammeras current position stored as Cam_Pos. This gives us the x, y and z.
		Vector3 Cam_Pos = transform.position;

		if (Input.GetKey ("w")|| Input.mousePosition.y >= Screen.height - Pan_Border) 
		{
			//move the camera in the Z axis by the pan speed, multiplied by the delta time for conistsant movement no matter the frame rate.
			Cam_Pos.z += Speed_Pan * Time.deltaTime;
		}
		if (Input.GetKey ("s")|| Input.mousePosition.y <= Pan_Border) 
		{
			//move the camera in the Z axis by the pan speed, multiplied by the delta time for conistsant movement no matter the frame rate.
			Cam_Pos.z -= Speed_Pan * Time.deltaTime;
		}
		if (Input.GetKey ("d")|| Input.mousePosition.x >= Screen.width - Pan_Border) 
		{
			//move the camera in the Z axis by the pan speed, multiplied by the delta time for conistsant movement no matter the frame rate.
			Cam_Pos.x += Speed_Pan * Time.deltaTime;
		}
		if (Input.GetKey ("a")|| Input.mousePosition.x <= Pan_Border) 
		{
			//move the camera in the Z axis by the pan speed, multiplied by the delta time for conistsant movement no matter the frame rate.
			Cam_Pos.x -= Speed_Pan * Time.deltaTime;
		}


		float Mouse_Scroll = Input.GetAxis ("Mouse ScrollWheel");
		Cam_Pos.y -= Mouse_Scroll * Speed_Scroll * 100.0f * Time.deltaTime; 


		// clamp allows us to limit a number between a minimum and a maximum Therefore we are limiting how far the camera can go based on the Pan_Limit
		Cam_Pos.x = Mathf.Clamp(Cam_Pos.x, -Pan_Limit.x, Pan_Limit.x);
		//Lock that y axis for scroll
		Cam_Pos.y = Mathf.Clamp (Cam_Pos.y, Min_Y, Max_Y);

		Cam_Pos.z = Mathf.Clamp (Cam_Pos.z, -Pan_Limit.y, Pan_Limit.y);

		//Feedback the new position to our Cam_Pos
		transform.position = Cam_Pos;


	}
	/*
	void LateUpdate ()
	{
		transform.position = FocusPoint.position - offset * DefaultZoom;
		transform.LookAt (FocusPoint.position + Vector3.up * pitch);
	}
	*/
}
