﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawning : MonoBehaviour {

	public GameObject[] ToBeSpawned;
	public Vector3 LocationValues;
	public float Spawn_wait;
	public int start_wait;
	int rescource;
	public int On_Screen = 0;


	// Use this for initialization
	void Start () {
		StartCoroutine (Spawn ());


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator Spawn ()
	{
		// Wait a certain amount of time before executing the code below it
		yield return new WaitForSeconds (start_wait);




			while (true) {
				rescource = Random.Range (0, 4);
				Vector3 S_Pos = new Vector3 (Random.Range (-LocationValues.x, LocationValues.x), 1, Random.Range (-LocationValues.z, LocationValues.z));
				Instantiate (ToBeSpawned [rescource], S_Pos + transform.TransformPoint (0, 0, 0), gameObject.transform.rotation);
				yield return new WaitForSeconds (Spawn_wait);
				On_Screen++;
			if (On_Screen == 5) 
			{
				yield break;
			}
			}
			

			}



	}

