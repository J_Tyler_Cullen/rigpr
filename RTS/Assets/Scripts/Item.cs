﻿using UnityEngine;

//This allows us to create scriptable Items from the create menu in Unity

[CreateAssetMenu (fileName = "New Item", menuName = "Inventory/Item")]

public class Item : ScriptableObject {

	new public string name = "New Item";
	public Sprite icon = null;
}
