﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(PlayMove))]

public class PlayerController : MonoBehaviour {
	//Filter out objects that is not the ground using a LayerMask

	public LayerMask MovMask;
	Camera cam;
	PlayMove move;
	public Interaction Focus;
    private PlayerController player;

	// Use this for initialization
	void Start () {
		cam = Camera.main;
		move = GetComponent<PlayMove> ();
		Cursor.visible = true;
    }
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) 
		{
			//Raycast from the camera to what has been clicked on

			Ray ray = cam.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			// If the raycast hits something, the code bellow executes
			// Add a Layermask to filter everything but the ground which is tagged in the inspector
			if (Physics.Raycast (ray, out hit,1000, MovMask)) 
			{
				// Test the Raycast is working by telling me what has been hit and where.
				//Debug.Log ("Object Hit " + hit.collider.name + " " + hit.point);

				//Move Player to what we hit
				move.MoveToClick(hit.point);

				//End Focusing on interactable 

				RemoveFocus();


			}
		}




		if (Input.GetMouseButtonDown (1)) 
		{
			//Raycast from the camera to what has been clicked on

			Ray ray = cam.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			// If the raycast hits something, the code bellow executes
			// Add a Layermask to filter everything but the ground which is tagged in the inspector
			if (Physics.Raycast (ray, out hit,1000)) 
			{
				// Test the Raycast is working by telling me what has been hit and where.
				//Debug.Log ("Object Hit " + hit.collider.name + " " + hit.point);

				 //See if we hit an interactable, if we did focus on it.

				Interaction interactable = hit.collider.GetComponent<Interaction> ();
				if (interactable != null) 
				{
                    Debug.Log("Setting ne focus");
					SetFocus (interactable);
				}

			}
		}
		
	}

	void SetFocus (Interaction NewFocus)
	{
		// Check to see if we are already focused

		if (NewFocus != Focus) 
		{

			if (Focus != null) 
				Focus.OnDefocused ();
			
			Focus = NewFocus;
			move.FollowClicked (NewFocus);
		}
			
		NewFocus.OnFocused (transform);

	}

	void RemoveFocus ()
	{
		if (Focus != null) 
			Focus.OnDefocused ();
		
		Focus = null;
		move.StopFollowingClicked();
	}
}
