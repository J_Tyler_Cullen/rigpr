﻿using UnityEngine;

public class ItemPickUp : Interaction {

	public Item item;

	public override void Interact()
	{
		base.Interact ();

		Pick ();
	}

	void Pick ()
	{


		Debug.Log ("Picking up " + item.name);
		//Add to Inventory
		ResourceInventory.instance.Add(item);


		// Destroy from Scene
		UnityEngine.Object.Destroy (gameObject);
	}
}
