﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRotate : MonoBehaviour {

	public float TurnSpeed = 50.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Q)) 
		{
			transform.Rotate (Vector3.up, -TurnSpeed * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.E)) 
		{
			transform.Rotate (Vector3.up, +TurnSpeed * Time.deltaTime);
		}
		
	}
}
