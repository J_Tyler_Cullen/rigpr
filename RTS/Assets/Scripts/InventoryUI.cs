﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour {

	ResourceInventory inventory;
	public Text txtR1;
	public Text txtR2;
	public Text txtR3;
	private int R1Count = 0;
	private int R2Count = 0;
	private int R3Count = 0;
	Collision col;
	public Image imgR1, imgR2, imgR3; 


	// Use this for initialization
	void Start () {
		inventory = ResourceInventory.instance;
		inventory.onItemChangedCallback += UpdateUI;

	}
	
	// Update is called once per frame
	void Update () {
		txtR1.text = "R1: " + R1Count;
		txtR2.text = "R2: " + R2Count;
		txtR3.text = "R3: " + R3Count;
	}




	void UpdateUI()
	{
		
			imgR1.enabled = true;
			R1Count += 1;
			Debug.Log ("UPDATING UI");

		
		/*
		imgR2.enabled = true;
		R2Count += 1;
		Debug.Log ("UPDATING UI");
		*/
	}
}
