﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveSpawner : MonoBehaviour {

	public enum SpawnState {SPAWNING, WAITING, COUNTING };

	[System.Serializable] 
	//Allows us to change the values inside of the unity inspector
	public class Wave
	{
		public string name;
		public Transform enemy;
		public int count;
		public float rate;

	}

	public Wave[] waves;
	private int NextWave = 0;

	public Transform[] SpawnPoints;

	public float TimeTillWave = 5f;
	public float waveCountdown;

	private float SearchTimer = 1f;

	public SpawnState state = SpawnState.COUNTING;

	void Start()
	{
		if (SpawnPoints.Length == 0) 
		{
			Debug.LogError ("No spawn points");
		}

		waveCountdown = TimeTillWave;
	}

	void Update()
	{
		if (state == SpawnState.WAITING) 
		{
			if (!EnemyIsAlive ()) {
				WaveCompleted ();
			}
			else 
			{
				return;
			}
		}


		if (waveCountdown <= 0) 
		{
			if (state != SpawnState.SPAWNING) 
			{
				StartCoroutine (SpawnWave (waves [NextWave]));
			}
		}
			else 
			{
				waveCountdown -= Time.deltaTime;
			}
	}


	void WaveCompleted ()

	{
		Debug.Log ("Wave Completed!");

		state = SpawnState.COUNTING;
		waveCountdown = TimeTillWave;

		if (NextWave + 1 > waves.Length - 1) {
			NextWave = 0;
			Debug.Log ("All Waves Complete. Looping...");
		} 
		else 
		{
			NextWave++;
		}

		NextWave++;
	}

	bool EnemyIsAlive ()
	{

		SearchTimer -= Time.deltaTime;

		if (SearchTimer <= 0f) 
		{
			SearchTimer = 1f;
			if (GameObject.FindGameObjectWithTag ("Enemy") == null) 
			{
				
				return false;
			}
		}
			
		return true;
	}

	IEnumerator SpawnWave(Wave _wave)
	{
		Debug.Log ("Spawning Wave: " + _wave.name);
		state = SpawnState.SPAWNING;

		for (int i = 0; i < _wave.count; i++) 
		{
			SpawnEnemy (_wave.enemy);
			yield return new WaitForSeconds (1f/_wave.rate);

		}

		state = SpawnState.WAITING;

		yield break;
	}

	void SpawnEnemy (Transform _enemy)
	{
		Debug.Log ("Spawning Enemy: " + _enemy.name);

		Transform _RandomSP = SpawnPoints [Random.Range (0, SpawnPoints.Length)];
		Instantiate (_enemy, _RandomSP.position, _RandomSP.rotation);
	}

}
 