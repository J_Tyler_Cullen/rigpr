﻿using UnityEngine;

public class Interaction : MonoBehaviour {

	public float radius = 10f;
	public Transform InteractionTransform; // Allows us to face a specific way when interacting
	bool isFocused = false;
	Transform player;

	bool hasInteracted = false;

	// Virtual will allow me to overide it when using this script on AI, while keeping everything else of the script
	public virtual void Interact()
	{
		Debug.Log ("Interacting with " + transform.name);
	}

	void Update()
	{
		// If we are focused, what is the distance to the player?

		if (isFocused && !hasInteracted) 
		{
			float distance = Vector3.Distance (player.position, InteractionTransform.position);
			if (distance <= radius) 
			{
				Interact ();
				hasInteracted = true;
			}
		}
	}


	public void OnFocused (Transform PlayerTransform)
	{
		isFocused = true;
		player = PlayerTransform;
		hasInteracted = false;
	}

	public void OnDefocused()
	{
		isFocused = false;
		player = null;
		hasInteracted = false;
	}


	//Draw Radius in Unity
	void OnDrawGizmosSelected()
	{
		if (InteractionTransform == null)
		{
			InteractionTransform = transform;
		}

		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere (InteractionTransform.position, radius);
	}

}
