﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchTower_Attack : MonoBehaviour {

	[Header ("Shooting")]

	public Transform target;
	public float range = 10f;
	public float FireRate = 1f;
	public float fireCountdown = 0f;

	[Header ("Setup")]
	//public Transform PartToRotate;
	public string EnemyTag = "Enemy";
	public GameObject bulletPrefab;
	public Transform firePoint;


	// Use this for initialization
	void Start () {
		InvokeRepeating ("UpdateTarget",0f,0.5f);	
	}


	void UpdateTarget()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag (EnemyTag);
		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		foreach (GameObject enemy in enemies) 
		{
			float DistanceToEnemy = Vector3.Distance (transform.position, enemy.transform.position);
			if (DistanceToEnemy < shortestDistance) 
			{
				shortestDistance = DistanceToEnemy;
				nearestEnemy = enemy;
			}
		}

		if (nearestEnemy != null && shortestDistance <= range) {
			target = nearestEnemy.transform;

		}
		else 
		{
			target = null;
		}
	}


	// Update is called once per frame
	void Update () {
		if (target == null)
			return;

		Vector3 dir = target.position - transform.position;
		//Quaternion lookRotation = Quaternion.LookRotation (dir);
		//Vector3 rotation = lookRotation.eulerAngles;
		//PartToRotate.rotation = Quaternion.Euler (0f, rotation.y, 0f);

		if (fireCountdown <= 0f) 
		{
			Shoot ();
			fireCountdown = 1f / FireRate;
		}

		fireCountdown -= Time.deltaTime;
	}


	void Shoot ()
	{
		GameObject bulletGO = (GameObject) Instantiate (bulletPrefab, firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGO.GetComponent<Bullet> ();

		if (bullet != null) 
		{
			bullet.Seek (target);
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, range);
	}
}
