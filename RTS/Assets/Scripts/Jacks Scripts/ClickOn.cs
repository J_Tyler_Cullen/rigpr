﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickOn : MonoBehaviour {

    [SerializeField]
    private Material red;
    [SerializeField]
    private Material Green;

    private MeshRenderer myRend;

	// Use this for initialization
	void Start () {
        myRend = GetComponent<MeshRenderer>();
    
	}
	
	public void Clickme()
    {
        myRend.material = Green;
    }

}
