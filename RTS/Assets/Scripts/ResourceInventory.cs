﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceInventory : MonoBehaviour {
	#region Singleton

	public static ResourceInventory instance;
	public delegate void OnItemChanged ();
	public OnItemChanged onItemChangedCallback;


	//Always allows us to acess this componant 

	void Awake()
	{

		if (instance != null) 
		{
			Debug.LogWarning ("Multiple instances of Inv");
			return;
		}

		instance = this;
	}

	#endregion

	public List<Item> items = new List <Item> ();

	public void Add( Item item)
	{
		items.Add (item);

		if (onItemChangedCallback != null)
			onItemChangedCallback.Invoke ();
	}

	// Incase we need it later
	public void Remove (Item item)
	{
		items.Remove (item);
	}
}
