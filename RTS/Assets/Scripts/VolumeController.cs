﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Needed to acess unity's UI elements
using UnityEngine.UI;

public class VolumeController : MonoBehaviour {
	// Name of the Slider (Volume Control)
	public Slider V_Control;

	// Name of the audio scource (Background Music)

	public AudioSource BG_M;
	
	// Update is called once per frame
	void Update () {
		// make the volume value equal to the slider value
		BG_M.volume = V_Control.value;
		
	}
}
