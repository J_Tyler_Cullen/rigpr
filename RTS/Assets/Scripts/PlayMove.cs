﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Make sure a NavMeshAgent is always needed

[RequireComponent(typeof(NavMeshAgent))]

public class PlayMove : MonoBehaviour {

	Transform target;

	NavMeshAgent agent;

	// Use this for initialization
	void Start () {

		agent = GetComponent<NavMeshAgent> (); 

	}


	void Update ()
	{
		if (target != null) 
		{
			agent.SetDestination (target.position);
			FaceTarget ();
		}
	}

	public void MoveToClick (Vector3 clicked)
	{


		// Player Moves to Clicked Area
		agent.SetDestination (clicked);
	}

	// When clicking an interactable, we want it follow it and not simply move towards it. This will be useful for AI

	public void FollowClicked (Interaction NewTarget)
	{
		agent.stoppingDistance = NewTarget.radius * .8f; // Make sure we get in the radius, not just outside of it.
		agent.updateRotation = false; // Fix rotation issue when following a interactable
		target = NewTarget.InteractionTransform;
	}

	public void StopFollowingClicked()
	{
		agent.stoppingDistance = 0f;
		agent.updateRotation = true;
		target = null;
	}

	void FaceTarget ()

	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion LookRotation = Quaternion.LookRotation (new Vector3 (direction.x, 0f, direction.z));
		transform.rotation = Quaternion.Slerp (transform.rotation, LookRotation, Time.deltaTime * 5f);
	}
}
