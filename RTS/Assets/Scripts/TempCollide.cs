﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempCollide : MonoBehaviour {

	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.name == "R1(Clone)") {
			Destroy (col.gameObject);
		}
		if (col.gameObject.name == "R2(Clone)") {
			Destroy (col.gameObject);
		}
		if (col.gameObject.name == "R3(Clone)") {
			Destroy (col.gameObject);
		}
	}
}
