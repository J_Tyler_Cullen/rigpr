﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

<<<<<<< HEAD
public class Resource : MonoBehaviour
{
=======
public class Resource : MonoBehaviour {
>>>>>>> 9939a44644c8bedb1bef0d2657bfb0b12b13ca5a

    public InventoryController inventory;
    public string resourceType;
    public int resourceStorage;
<<<<<<< HEAD
    private int maxStorage;
    private bool treeChanged1 = false, treeChanged2 = false;
    //public bool inventoryFull = false;


    // Use this for initialization
    void OnEnable()
    {
        inventory = GameObject.FindGameObjectWithTag("GameManager").gameObject.GetComponent<InventoryController>();
        maxStorage = resourceStorage;
    }

    // Update is called once per frame
    void Update()
    {
        if (resourceType == "Stone")
        {
            if (resourceStorage <= 0)
            {
                Destroy(gameObject);
            }
            else if (resourceStorage <= maxStorage / 3)
            {
                gameObject.GetComponent<MeshFilter>().mesh = inventory.emptyStone.GetComponent<MeshFilter>().sharedMesh;
            }
            else if (resourceStorage <= (2 * (maxStorage / 3)))
            {
                gameObject.GetComponent<MeshFilter>().mesh = inventory.halfStone.GetComponent<MeshFilter>().sharedMesh;
            }
        } else if (resourceType == "Wood")
        {
            if (resourceStorage <= 0)
            {
                Destroy(gameObject);
            }
            else if (resourceStorage <= maxStorage / 3 && !treeChanged2)
            {
                Vector3 tempLocation = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
                GameObject tempObject = Instantiate(inventory.emptyWood, tempLocation, gameObject.transform.rotation);
                StopCoroutine(inventory.Wood);
                tempObject.GetComponent<Resource>().resourceStorage = resourceStorage;
                //treeChanged2 = true;
                tempObject.GetComponent<Resource>().treeChanged1 = true;
                tempObject.GetComponent<Resource>().treeChanged2 = true;
                Destroy(gameObject);
            }
            else if (resourceStorage <= (2 * (maxStorage / 3)) && !treeChanged1)
            {
                Vector3 tempLocation = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
                GameObject tempObject = Instantiate(inventory.halfWood, tempLocation, gameObject.transform.rotation);
                StopCoroutine(inventory.Wood);
                tempObject.GetComponent<Resource>().resourceStorage = resourceStorage;
                //treeChanged1 = true;
                tempObject.GetComponent<Resource>().treeChanged1 = true;
                Destroy(gameObject);
                
                //gameObject.GetComponent<MeshFilter>().mesh = inventory.halfWood.GetComponent<MeshFilter>().sharedMesh;
            }
        } else if (resourceType == "Food")
        {
            if (resourceStorage <= 0)
            {
                Destroy(gameObject);
            }
            else if (resourceStorage <= maxStorage)
            {
                gameObject.GetComponent<MeshFilter>().mesh = inventory.emptyFood.GetComponent<MeshFilter>().sharedMesh;
                //Destroy(gameObject.GetComponent<Animation>());
            }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        //collision.gameObject.GetComponent<Collider>().enabled = true;
        Debug.Log("Collided with Stone");
        inventory.resource = gameObject.GetComponent<Resource>();
        if (collider.gameObject.name == "Player" && !inventory.inventoryFull)
        {
            Debug.Log("starting resource gathering");

            if (resourceType == "Stone")
            {
                inventory.Stone = StartCoroutine(inventory.MiningStone(inventory.collectInterval));
            }
            else if (resourceType == "Food")
            {
                inventory.Food = StartCoroutine(inventory.GatheringFood(inventory.collectInterval));
            }
            else if (resourceType == "Wood")
            {
                inventory.Wood = StartCoroutine(inventory.ChoppingWood(inventory.collectInterval));
            }
        }

    }

    void OnTriggerExit(Collider collider)
    {
        collider.enabled = true;
    }
=======


    // Use this for initialization
    void OnEnable() {
        
	}
	
	// Update is called once per frame
	void Update () {
		if(resourceType == "Stone" && resourceStorage <= (resourceStorage/2))
        {
            //change model
        }
	}

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collided with Stone");
        if (resourceType == "Stone")
        {
            StartCoroutine(inventory.Stone);
        }
        else if (resourceType == "Food")
        {
            Destroy(collision.gameObject);
        }
        else if (resourceType == "Wood")
        {
            Destroy(collision.gameObject);
        }
    }

    void OnCollisionExit(Collision collision) {
        StopCoroutine(inventory.Stone);
    }

>>>>>>> 9939a44644c8bedb1bef0d2657bfb0b12b13ca5a
}
