﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TakeDamage : MonoBehaviour {

    public Slider playerHealth;
    public GameObject healthHolder;
    public GameObject mainCamera;

	// Use this for initialization
	void Start () {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
        playerHealth.transform.LookAt(mainCamera.transform);
        if(playerHealth.value <= 0)
        {
            //kill the player (Destroy or something)
        }
	}

    public void TakeDealDamageDamage()
    {
        playerHealth.value -= 1;
    }
}
