﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Selecting : MonoBehaviour
{
    private GameObject tempObject;
    public List<GameObject> selectableUnits;
    public Camera mainCamera;
    private GameObject selectedUnit;
    public List<GameObject> upgradeParents;
    

    // Use this for initialization
    void Start()
    {
        //selectableUnits.Add(player);
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo) && Input.GetMouseButton(0))
        {
            tempObject = hitInfo.collider.gameObject;
            tempObject.GetComponent<Collider>().enabled = false;
            mainCamera.transform.position = new Vector3(tempObject.transform.position.x, mainCamera.transform.position.y, (tempObject.transform.position.z)-50.0f);
            //tempObject.GetComponent<Collider>().enabled = true;
            //ChangeUI();
        }
    }

    public void ChangeUI()
    {
        string buildingName;
        buildingName = tempObject.GetComponent<BuildingInfo>().buildingName;
        for (int i = 0; i < upgradeParents.Count; i++)
        {
            upgradeParents[i].SetActive(false);
        }

        if (buildingName == "Stronghold")
        {
            upgradeParents[0].SetActive(true);
        } else if (buildingName == "Barracks")
        {
            upgradeParents[1].SetActive(true);
        }
    }
}
