﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingInfo : MonoBehaviour {

<<<<<<< HEAD
    public InventoryController inventory;
    public string buildingName;
    public int storage, maxStorage;
    private int storageDifference;
    // Use this for initialization
    void OnEnable () {
        inventory = GameObject.FindGameObjectWithTag("GameManager").gameObject.GetComponent<InventoryController>();
    }
	
	// Update is called once per frame
	void Update () {
        
        
    }

    private void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Depositing Material");
        if (buildingName == "Stone Storage")
        {
            storage += collider.gameObject.GetComponent<Player>().stoneCollected;
            storageDifference = maxStorage - storage;

            if (storageDifference <= 0)
            {
                inventory.stone += (collider.gameObject.GetComponent<Player>().stoneCollected + storageDifference);
                storage = maxStorage;
            } else
            {
                inventory.stone += collider.gameObject.GetComponent<Player>().stoneCollected;
            }
        } else if (buildingName == "Wood Storage")
        {
            storage += collider.gameObject.GetComponent<Player>().woodCollected;
            storageDifference = maxStorage - storage;

            if (storageDifference <= 0)
            {
                inventory.wood += (collider.gameObject.GetComponent<Player>().woodCollected + storageDifference);
                storage = maxStorage;
            }
            else
            {
                inventory.wood += collider.gameObject.GetComponent<Player>().woodCollected;
            }
        }
        else if (buildingName == "Food Storage")
        {
            storage += collider.gameObject.GetComponent<Player>().foodCollected;
            storageDifference = maxStorage - storage;

            if (storageDifference <= 0)
            {
                inventory.food += (collider.gameObject.GetComponent<Player>().foodCollected + storageDifference);
                storage = maxStorage;
            }
            else
            {
                inventory.food += collider.gameObject.GetComponent<Player>().foodCollected;
            }
        }

        collider.gameObject.GetComponent<Player>().stoneCollected = 0;
        collider.gameObject.GetComponent<Player>().woodCollected = 0;
        collider.gameObject.GetComponent<Player>().foodCollected = 0;
        inventory.inventoryFull = false;
        inventory.UpdateResourceValues();
    }
=======
    public string buildingName;
    public Button[] upgrades;
	// Use this for initialization
	void OnEnable () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
>>>>>>> 9939a44644c8bedb1bef0d2657bfb0b12b13ca5a
}
