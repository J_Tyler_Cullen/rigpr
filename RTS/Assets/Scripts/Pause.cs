﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

	public Transform Pausecanvas;
	public Transform MiniMapcanvas;
	public Transform Resourcecanvas;

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown ("escape")) 
		{
			PauseF ();
		}

		
	}

	public void PauseF()
	{



			Time.timeScale = 0;

			if (Pausecanvas.gameObject.activeInHierarchy == false) {

				Pausecanvas.gameObject.SetActive (true);
				MiniMapcanvas.gameObject.SetActive (false);
				Resourcecanvas.gameObject.SetActive (false);

			} 
			else 
			{

				Time.timeScale = 1;
				Pausecanvas.gameObject.SetActive (false);
				MiniMapcanvas.gameObject.SetActive (true);
				Resourcecanvas.gameObject.SetActive (true);
			}
		
	}
}
