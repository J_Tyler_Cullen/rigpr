﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {


		public Transform target;
		public Vector2 rotRange;
		public bool smoothRot;
		public float followDelay = 1;

		Vector3 followAngles;

		Vector3 refVelocity;

		Vector3 localTarget,targetAngles;
		float yAngle,xAngle;

		void Update()
		{
			if(target!=null)
			{
				transform.localRotation=Quaternion.identity;

				localTarget = transform.InverseTransformPoint(target.position);

				yAngle = Mathf.Atan2(localTarget.x, localTarget.z)*Mathf.Rad2Deg;
				yAngle = Mathf.Clamp(yAngle, -rotRange.y*.5f, rotRange.y*.5f);

				transform.localRotation=Quaternion.Euler(0,yAngle,0);

				localTarget = transform.InverseTransformPoint(target.position);

				xAngle = Mathf.Atan2(localTarget.y, localTarget.z)*Mathf.Rad2Deg;
				xAngle = Mathf.Clamp(xAngle, -rotRange.x*.5f, rotRange.x*.5f);

				if(smoothRot)
				{
					targetAngles = new Vector3(followAngles.x + Mathf.DeltaAngle(followAngles.x, xAngle),
						followAngles.y + Mathf.DeltaAngle(followAngles.y, yAngle));

					followAngles = Vector3.SmoothDamp(followAngles, targetAngles, ref refVelocity, followDelay);

					transform.localRotation = Quaternion.Euler(-followAngles.x, followAngles.y, 0);
				}
				else
				{
					targetAngles=new Vector3(xAngle,yAngle);
					transform.localRotation = Quaternion.Euler(-targetAngles.x, targetAngles.y, 0);
				}
			}
		}
	}