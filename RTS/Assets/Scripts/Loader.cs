﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MonoBehaviour {

	public GameObject LoadScreen;
	public Slider slider;
	public Text progress_txt;

	public void L_Level(int S_Index)
	{
		StartCoroutine (LoadAsynchronously (S_Index));
	}
	IEnumerator LoadAsynchronously (int S_Index)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (S_Index);
		LoadScreen.SetActive (true);

		while (!operation.isDone) 
		{
			float L_Progress = Mathf.Clamp01 (operation.progress / .9f);
			slider.value = L_Progress;
			progress_txt.text = L_Progress * 100f + "%";

			//wait till the next frame before continuing

			yield return null;
		}
	}
}
