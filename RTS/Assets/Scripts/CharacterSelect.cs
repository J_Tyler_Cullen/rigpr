﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelect : MonoBehaviour {

	private GameObject[]CharacterList;
	private int index;

	private void Start()
	{

		index = PlayerPrefs.GetInt ("CharacterSelected");
		CharacterList = new GameObject[transform.childCount];

		//Fill the array with our characters

		for (int i = 0; i < transform.childCount; i++) 
		{
			CharacterList [i] = transform.GetChild (i).gameObject;
		}

		//Toggle off their render

		foreach (GameObject go in CharacterList) 
		{
			go.SetActive (false);
		}

		// Toggle on the selected character in the index
		if (CharacterList [index]) 
		{
			CharacterList [index].SetActive (true);
		}
	}


	public void left()
	{
		//Toggle off character
		CharacterList[index].SetActive(false);

		index--;
		if (index < 0) 
		{
			index = CharacterList.Length - 1;
		}
		//toggle on the new character
		CharacterList[index].SetActive(true);
	}

	public void right()
	{
		//Toggle off character
		CharacterList[index].SetActive(false);

		index++;
		if (index == CharacterList.Length) 
		{
			index = 0;
		}
		//toggle on the new character
		CharacterList[index].SetActive(true);
	}

	public void Confirm()
	{
		PlayerPrefs.SetInt ("CharacterSelected", index);
		SceneManager.LoadScene ("Main_Game");
	}
}
