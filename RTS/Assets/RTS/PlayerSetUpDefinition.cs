﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerSetUpDefinition  {


    public string Name;
    public Transform Location;
    public Color AcentColor;
    public List<GameObject> StartingUnits = new List<GameObject>();
    public bool IsAI;
    public float Credits;
   // public List<GameObject> StartingUnits = new List<GameObject>();

    private List<GameObject> activeUnits = new List<GameObject>();

    public List<GameObject> ActiveUnits { get { return activeUnits; } }

}
