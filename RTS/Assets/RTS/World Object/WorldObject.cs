﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObject : MonoBehaviour {

	public string objectName;
	public Texture2D buildImage;
	public int cost, sellValue, hitPoints, maxHitPoints;
	protected Player player;
	protected string[] actions = {};
	protected bool currentlySelected = false;

	protected virtual void Awake() {

	}

	protected virtual void Start () {
		player = gameObject.GetComponent< Player >();
	}

	protected virtual void Update () {

	}

	//We may not need the OnGUI

	protected virtual void OnGUI() {
	}

	public void SetSelection(bool selected) 
	{
		currentlySelected = selected;
	}

	public string[] GetActions() 
	{
		return actions;
	}

	public virtual void PerformAction(string actionToPerform) 
	{
		//it is up to children with specific actions to determine what to do with each of those actions
	}
}
