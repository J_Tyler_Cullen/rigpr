﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {
    public string PlayerName;
    public float Confusion = 0.3f;
    public float Frequency = 1;
    private float waited = 0;
    public static AIController current = null;
    private PlayerSetupDefinition player;
    
    public List<PlayerSetupDefinition> Players = new List<PlayerSetupDefinition>();

    private List<AIBehaviour> AIs = new List<AIBehaviour>();

    public PlayerSetupDefinition Player{get { return player; }}
	// Use this for initialization
	void Start () {
        current = this;

        foreach (var ai in GetComponents<AIBehaviour>())
            AIs.Add(ai);

        foreach (var p in AIController.current.Players)
        {
            if (p.Name == PlayerName)
            {
                player = p;
            }

        }
		
	}
	
	// Update is called once per frame
	void Update () {
        waited += Time.deltaTime;
        if (waited < Frequency)
            return;



        float bestAIValue = float.MinValue;
        AIBehaviour bestAi = null;

        foreach (var ai in AIs)
        {
            ai.TimePassed += waited;
            var aiValue = ai.GetWeigtht() * ai.WeightMultiplier + Random.Range(0, Confusion);
            if (aiValue > bestAIValue)
            {
                bestAIValue = aiValue;
                bestAi = ai;
            }
        }

        bestAi.Execute();
        waited = 0f;


	}
}
