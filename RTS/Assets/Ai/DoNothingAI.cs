﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNothingAI : AIBehaviour {
    public float ReturnWeight = 0.5f;
    public override float GetWeigtht()
    {
        return ReturnWeight;
    }
    public override void Execute()
    {
        Debug.Log("Doing Nothing");
    }
}
