﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIBehaviour : MonoBehaviour {
    public float WeightMultiplier = 1;
    public float TimePassed = 0;
    public abstract float GetWeigtht();
    public abstract void Execute();
}
